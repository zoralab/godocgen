package godocgen

const (
	headerConstant = "Constants"
	headerFunction = "Functions"
	headerNone     = ""
	headerTitle    = "Package"
	headerVariable = "Variables"
	headerExample  = "Example"
)

const (
	testMode                        = 0
	testSimulateFailedABSFilepath   = 1
	testSimulateFailedABSOutput     = 2
	testSimulateBadStatOutput       = 3
	testSimulateGeneratorError      = 4
	testSimulateFailedFileCreation  = 5
	testSimulateFailedCanvaCreation = 6
)

const (
	errorSimulateBadStatOutput  = "simulate bad stat output path error"
	errorSimulateGeneratorError = "simulate generator has error"

	testTerminalOutputPath = "./test/outputs/terminal.txt"
)
