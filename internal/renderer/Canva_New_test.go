package renderer

import (
	"testing"
)

func TestNew(t *testing.T) {
	scenarios := testScenarios()
	for i, s := range scenarios {
		if s.TestType != testNew {
			continue
		}

		// prepare
		th := s.PrepareTHelper(t)
		w := s.CreateWriter(th)

		var tPath, docType string
		var width uint
		var c *Canva
		var err error

		if w == nil {
			goto verifyTest
		}

		tPath, docType, width = s.CreateCanvaParameters()

		// test
		c, err = New(w.Writer, tPath, docType, width)

		w.Close()

	verifyTest:
		s.ScanNewOutput(th, c, docType)
		s.ScanNewError(th, err)
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.Log(th, map[string]interface{}{
			"Template Path": tPath,
			"Doc Type":      docType,
			"Width":         width,
			"Canva":         c,
			"Error":         err,
		})
		th.Conclude()
	}
}
