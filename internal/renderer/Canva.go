package renderer

import (
	"fmt"
	"io"
	"path/filepath"
	"strings"
)

type renderFunctions interface {
	reset() (err error)
	renderHead(name string, synopsis string, relativeName string)
	renderTitle(pkg string, name string)
	renderHeader(name string)
	renderSynopsis(synopsis string)
	renderDeclGroup(name string)
	renderDeclElements(header string, value string)
	renderEndDeclGroup()
	renderFunction(header string, receiver string, value string)
	renderCode(code []string)
	renderNewLine()
	renderImportPath(path string)
	renderExample(name string,
		suffix string,
		doc string,
		codes []string,
		output string)
}

// IsSupportedFormat is to check requested document format is supported.
//
// This is used for checking the request before creating the `os.file` for
// Canva to write.
func IsSupportedFormat(path string) (docType string) {
	docType = filepath.Ext(path)
	docType = strings.TrimPrefix(docType, ".")

	switch {
	case path == DocTypeTerminal:
		docType = DocTypeTerminal
	case docType == DocTypeMarkdown:
		docType = DocTypeMarkdown
	case docType == DocTypeText:
		docType = DocTypeText
	default:
		docType = DocTypeNone
	}

	return docType
}

// Canva is the renderer data structure that holds the renderer's engine.
//
// It has private variables that require initialization and uses certain inputs
// to configure its engine. Hence, please **strictly** use `renderer.New()` to
// create a new Canva structure object.
type Canva struct {
	engine renderFunctions
}

// New is to create a new Canva object.
//
// The function returns either the Canva object or error object. The error is
// caused by incompatible values received from the parameters.
func New(writer io.Writer,
	templatePath string,
	docType string,
	width uint) (*Canva, error) {
	var err error

	c := &Canva{}

	switch {
	case docType == DocTypeMarkdown,
		docType == DocTypeText,
		docType == DocTypeTerminal && templatePath != "":
		c.engine = &textParser{
			buffer:       writer,
			width:        width,
			templatePath: templatePath,
		}
	case docType == DocTypeTerminal:
		c.engine = &terminal{
			buffer: writer,
			width:  width,
		}
	default:
		return nil, fmt.Errorf("unsupported document type")
	}

	err = c.engine.reset()
	if err != nil {
		return nil, err
	}

	return c, nil
}

// PraseHead is to render the package front-matter information.
//
// It is used to render the front-matter leading information for the
// documentation.
func (c *Canva) ParseHead(name string, synopsis string, relativeName string) {
	c.engine.renderHead(name, synopsis, relativeName)
}

// ParseTitle is to render the package title for the documentation.
//
// It is used to render the document's title.
func (c *Canva) ParseTitle(pkg string, name string) {
	c.engine.renderTitle(pkg, name)
}

// ParseHeader is to render the header section of a content.
//
// It is used to render the section's title.
func (c *Canva) ParseHeader(name string) {
	c.engine.renderHeader(name)
}

// ParseSynopsis is to render the document body of a content.
//
// It is used to render the section body's articles.
func (c *Canva) ParseSynopsis(synopsis string) {
	c.engine.renderSynopsis(synopsis)
}

// BeginDeclGroup is to render the starting indicator for a declarations group.
//
// This is commonly used in constant and variable groups.
func (c *Canva) BeginDeclGroup(name string) {
	c.engine.renderDeclGroup(name)
}

// PraseDeclElements is to render each elements in a declarations group.
//
// This is commonly used in constant and variable groups.
func (c *Canva) ParseDeclElements(header string, value string) {
	c.engine.renderDeclElements(header, value)
}

// EndDeclGroup is to render the ending indicator for a declarations group.
//
// This is commonly used in constant and variable groups.
func (c *Canva) EndDeclGroup() {
	c.engine.renderEndDeclGroup()
}

// ParseFunction is to render a function.
//
// This is commonly used in rendering generic function or type associated
// function.
func (c *Canva) ParseFunction(header string, receiver string, value string) {
	c.engine.renderFunction(header, receiver, value)
}

// ParseCode is to render a given code block.
//
// This is commonly used for type rendering (e.g. struct codes).
func (c *Canva) ParseCode(codes []string) {
	c.engine.renderCode(codes)
}

// ParseNewLine is to render a new line.
//
// This is commonly used for separator.
func (c *Canva) ParseNewLine() {
	c.engine.renderNewLine()
}

// ParseImportPath is the render import path statement.
//
// This is commonly used for stating the Go's import instruction to use the Go
// package module.
func (c *Canva) ParseImportPath(path string) {
	c.engine.renderImportPath(path)
}

// ParseExample is to render example function.
//
// This is commonly used for displaying example codes for various entities like
// package examples, function examples, type examples, and method examples.
func (c *Canva) ParseExample(name string,
	suffix string,
	doc string,
	codes []string,
	output string) {
	c.engine.renderExample(name, suffix, doc, codes, output)
}
