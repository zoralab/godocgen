package renderer

import (
	"testing"
)

func TestParse(t *testing.T) {
	scenarios := testScenarios()

	for i, s := range scenarios {
		if s.TestType != testParse {
			continue
		}

		// prepare
		th := s.PrepareTHelper(t)

		// test
		s.ExecuteSupportedFormat(th)

		// verify
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.Log(th, map[string]interface{}{})
		th.Conclude()
	}
}
