package generator

import (
	"os"
	"testing"

	"gitlab.com/zoralab/cerigo/testing/thelper"
)

const (
	badTestPackage   = "m3M2B$pdkzRCrDdp7Ye^Y&jBExsgf*zyu"
	emptyTestPackage = "./testempty"
	goodTestPackage  = "."
)

const (
	badGoMod     = "aewhgaergaeg.txt"
	goodGoMod    = "good_mod.txt"
	invalidGoMod = "invalid_mod.txt"
)

const (
	testConfigure = "testConfigure"
	testGenerate  = "testGenerate"
)

const (
	improperPackagePath       = "improperPackagePath"
	insertBadPath             = "insertBadPath"
	properPackagePath         = "properPackagePath"
	useGoodGoMod              = "useGoodGoMod"
	useBadGoMod               = "useBadGoMod"
	useInvalidGoMod           = "useInvalidGoMod"
	useEmptyPackagePath       = "useEmptyPackagePath"
	simulateFailedFileParsing = "simulateFailedFileParsing"
)

type testScenario thelper.Scenario

func (s *testScenario) prepareTHelper(t *testing.T) *thelper.THelper {
	return thelper.NewTHelper(t)
}

func (s *testScenario) log(th *thelper.THelper, data map[string]interface{}) {
	th.LogScenario(thelper.Scenario(*s), data)
}

func (s *testScenario) prepareTestEnvironment() {
	_ = os.MkdirAll(emptyTestPackage, os.ModePerm)
	_ = os.MkdirAll(badGoMod, os.ModePerm)
}

func (s *testScenario) createPath() (path string, verdict bool) {
	switch {
	case s.Switches[improperPackagePath]:
		path = badTestPackage
		verdict = false
	case s.Switches[insertBadPath]:
		path = emptyTestPackage
		verdict = false
	case s.Switches[useEmptyPackagePath]:
		path = ""
		verdict = false
	case s.Switches[properPackagePath]:
		fallthrough
	default:
		path = goodTestPackage
		verdict = true
	}

	return path, verdict
}

func (s *testScenario) configureGoMod(x *DocData, verdict bool) bool {
	switch {
	case s.Switches[useBadGoMod]:
		x.testGoMod = badGoMod
	case s.Switches[useInvalidGoMod]:
		x.testGoMod = invalidGoMod
	case s.Switches[useGoodGoMod]:
		fallthrough
	default:
		x.testGoMod = goodGoMod
	}

	if s.Switches[simulateFailedFileParsing] {
		x.testFailedFileParsing = true
		return false
	}

	return verdict
}

func (s *testScenario) cleanUp() {
	os.RemoveAll(emptyTestPackage)
	os.RemoveAll(badGoMod)
}
