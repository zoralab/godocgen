package generator

import (
	"testing"
)

func TestGeneratorConfigure(t *testing.T) {
	scenarios := getTestScenarios()

	for i, s := range scenarios {
		if s.TestType != testConfigure {
			continue
		}

		// prepare
		th := s.prepareTHelper(t)
		path, verdict := s.createPath()

		// test
		g := &DocData{}
		ret := g.Configure(path)

		// verdict
		th.ExpectSameBool("expect", verdict, "got", ret)
		th.ExpectExists("build package", g.BuildPackage != nil, verdict)
		th.ExpectExists("name", g.Name != "", verdict)
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.log(th, map[string]interface{}{
			"expected verdict": verdict,
			"given path":       path,
			"got verdict":      ret,
			"got package":      g.BuildPackage,
			"got name":         g.Name,
		})
		th.Conclude()
	}
}
