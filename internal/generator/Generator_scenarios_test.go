package generator

func getTestScenarios() []testScenario {
	return []testScenario{
		{
			UID:      1,
			TestType: testConfigure,
			Description: `
DocData.Configure() is able to run properly when:
1. a correct path is given.
2. true verdict is expected.
`,
			Switches: map[string]bool{
				properPackagePath: true,
			},
		}, {
			UID:      2,
			TestType: testConfigure,
			Description: `
DocData.Configure() is able to run properly when:
1. a bad path is given.
2. false verdict is expected.
`,
			Switches: map[string]bool{
				improperPackagePath: true,
			},
		}, {
			UID:      3,
			TestType: testGenerate,
			Description: `
DocData.Generate() is able to run properly when:
1. a good path is given.
`,
			Switches: map[string]bool{
				properPackagePath: true,
			},
		}, {
			UID:      4,
			TestType: testGenerate,
			Description: `
DocData.Generate() is able to run properly when:
1. a bad path is given.
`,
			Switches: map[string]bool{
				properPackagePath: true,
			},
		}, {
			UID:      5,
			TestType: testGenerate,
			Description: `
DocData.Generate() is able to run properly when:
1. a bad path is forcefully inserted.
`,
			Switches: map[string]bool{
				insertBadPath: true,
			},
		}, {
			UID:      6,
			TestType: testGenerate,
			Description: `
DocData.Generate() is able to run properly when:
1. a good go mod is used.
`,
			Switches: map[string]bool{
				properPackagePath: true,
				useGoodGoMod:      true,
			},
		}, {
			UID:      7,
			TestType: testGenerate,
			Description: `
DocData.Generate() is able to run properly when:
1. a good go mod is used.
`,
			Switches: map[string]bool{
				properPackagePath: true,
				useBadGoMod:       true,
			},
		}, {
			UID:      8,
			TestType: testGenerate,
			Description: `
DocData.Generate() is able to run properly when:
1. a good go mod is used.
`,
			Switches: map[string]bool{
				properPackagePath: true,
				useInvalidGoMod:   true,
			},
		}, {
			UID:      9,
			TestType: testGenerate,
			Description: `
DocData.Generate() is able to run properly when:
1. an empty path is provided.
`,
			Switches: map[string]bool{
				useEmptyPackagePath: true,
			},
		}, {
			UID:      10,
			TestType: testGenerate,
			Description: `
DocData.Generate() is able to run properly when:
1. a proper path is given.
2. simulate bad file AST parsing
`,
			Switches: map[string]bool{
				properPackagePath:         true,
				simulateFailedFileParsing: true,
			},
		},
	}
}
