package testpkg_test

import (
	"fmt"
)

// This example demonstrates how to use Person Type
func ExamplePerson() { //nolint:govet
	fmt.Printf("This is A Person Type Example\n")
	// Output: This is A Person Type Example
}

// This example demonstrates Person Type' Special Usage
func ExamplePerson_second() { //nolint:govet
	fmt.Printf("This is A Person second example\n")
	// Output: This is A Person second example
}

// This example demonstrates another Person Type's Special Usage
func ExamplePerson_Third() { //nolint:govet
	fmt.Printf("This is A Person Third example\n")
	// Output: This is A Person Third example
}

func ExamplePerson_fourth() { //nolint:govet
	// purposely forget to document
	fmt.Printf("This is A Person fourth example\n")
	// Output: This is A Person fourth example
}

// Example of demonstrating the use of NewPerson()
func ExampleNewPerson() { //nolint:govet
	fmt.Printf("This is newPerson\n")
	// Output: This is newPerson
}

// Example of demonstrating the use of NewPerson() II
func ExampleNewPerson_second() { //nolint:govet
	fmt.Printf("This is newPerson II\n")
	// Output: This is newPerson II
}

// Example of demonstrating the use of NewPerson() III
func ExampleNewPerson_Third() { //nolint:govet
	fmt.Printf("This is newPerson III\n")
	// Output: This is newPerson III
}

func ExampleNewPerson_fourth() { //nolint:govet
	// purposely forgotten to document
	fmt.Printf("This is newPerson IV\n")
	// Output: This is newPerson IV
}

// Demonstrate how to use Person.Greet("Name") I
func ExamplePerson_Greet() { //nolint:govet
	fmt.Printf("This is Person Greet I\n")
	// Output: This is Person Greet I
}

// Demonstrate how to use Person.Greet("Name") II
func ExamplePerson_Greet_second() { //nolint:govet
	fmt.Printf("This is Person Greet II\n")
	// Output: This is Person Greet II
}

// Demonstrate how to use Person.Greet("Name") III
func ExamplePerson_Greet_Third() { //nolint:govet
	fmt.Printf("This is Person Greet III\n")
	// Output: This is Person Greet III
}

func ExamplePerson_Greet_fourth() { //nolint:govet
	// purposely forgotten to document
	fmt.Printf("This is Person Greet IV\n")
	// Output: This is Person Greet IV
}
