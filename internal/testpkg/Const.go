package testpkg

// Greeting sentences
const (
	// HelloWorld is to greet the world!
	HelloWorld = "Hello World!"

	MissingLabel = "Constant without label"

	// NormalText is another text
	NormalText = "Normal!"
)

const (
	// No1 is inside a constant group forgotten to be described!
	No1 = "Number 1!"
)
