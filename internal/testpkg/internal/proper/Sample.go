package proper

// Sample is the inner package of testpkg for testing recursive parsing
func Sample() string {
	return "This is to show the inner package is sampled"
}
