// Copyright statements

// package proper is an internal package to testpkg for recursive testing.
//
// If we do not create this internal package, we cannot tell whether recursive
// function is working or otherwise.
//
// Proper package is an internal package which is **exclusive only** to
// testpkg to import from. External third-party importing is prohibited by Go
// build system.
package proper
