package testpkg

import (
	"testing"
)

func TestSayHello(t *testing.T) {
	s := SayHello()

	t.Logf("SayHello() returned %s", s)

	if s != HelloWorld {
		t.Errorf("[ FAILED ] SayHello() did not return proper output.")
		return
	}
}

func TestGreet(t *testing.T) {
	s := Greet("Sam")

	t.Logf("Given \"Sam\", Got: %s", s)

	if s != "Hello Sam" {
		t.Errorf("[ FAILED ] Greet(\"Sam\") did not return properly!")
	}

	s = Greet("")
	t.Logf(" Given \"\", Got: %s", s)

	if s != HelloWorld {
		t.Errorf("[ FAILED ] Greet(\"\") did not return proper output.")
	}
}

func TestWhatIs(t *testing.T) {
	WhatIs("This")
}

func TestPerson(t *testing.T) {
	p := NewPerson()

	s := p.Greet("Mark")
	if s != "Hello Mark! My name is John. How are you?" {
		t.Errorf("[ FAILED ] p.Greet(\"Mark\") returns: %s", s)
	}

	s = p.SayGoodbye("Mark")
	if s != "Looks like I need to go now. See ya, Mark!" {
		t.Errorf("[ FAILED ] p.SayGoodbye(\"Mark\") returns: %s", s)
	}
}
