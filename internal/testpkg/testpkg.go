package testpkg

import (
	"fmt"
)

// SayHello is to greet the world and initiate a conversation.
//
// This function is generally used across everyone who is curious to know
// another.
func SayHello() string {
	return HelloWorld
}

// Greet is to greet a specific person and initiate a conversation with him/her.
//
// This function is specifically used for interacting with an individual to
// query and conversate.
func Greet(name string) string {
	if name == "" {
		return HelloWorld
	}

	return "Hello " + name
}

func WhatIs(this interface{}) {
	// forget to document this function on purpose
	fmt.Printf("This is: %#v\n", this)
}

// Person is a structure holding a person's details
//
// This structure is safe to create using the conventional &Person{} method.
// Alternatively, you may use NewPerson() function.
type Person struct {
	// holds the name of the person
	Name string

	// holds the date of birth
	DOB string

	Phone string
}

// NewPerson is to create a new Person object.
//
// It returns the object pointer as a result.
func NewPerson() *Person {
	return &Person{
		Name: "John",
	}
}

// Greet is to make the person to greet someone else.
//
// This is to initiate conversation with an individual.
func (p *Person) Greet(name string) string {
	return fmt.Sprintf("Hello %s! My name is %s. How are you?",
		name,
		p.Name)
}

func (p *Person) SayGoodbye(name string) string {
	// forget to document this function on purpose
	return "Looks like I need to go now. See ya, " + name + "!"
}
