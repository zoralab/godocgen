package testpkg_test

import (
	"fmt"
)

// Second Package Example
func Example_second() {
	fmt.Println("Second Package Example")
	// Output: Second Package Example
}

// Third Package Example
func Example_Third() { //nolint:govet
	fmt.Println("Third Package Example")
	// Output: Third Package Example
}

func Example_fourth() { //nolint:govet
	// purposely forget to document
	fmt.Println("Fourth Package Example")
	// Output: Fourth Package Example
}

func Example_fifth() {} //nolint:govet
