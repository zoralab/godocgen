# Description
Describe for your question here. Leave your one-liner question in the Title.
Please make sure you leave an empty line before those 2 horizontal lines below.



## Severity
Level of Security: ***Significant***

<!--
OPTIONS

***Critical***
danger, hang, freeze, or kill computer

***Severe***
blocking and can't use. Can't workaround it.

***Significant***
quite a problem but still usable. Can workaround it.

***Not significant***
just some minor touch-up. Everything is fine.
-->



# Urgency
Level of Urgency: ***Code BLUE***

<!--
OPTIONS

***Code BLACK***
Somebody's life is at stake (e.g. medical equipment, national security)

***Code RED***
Immediate Attention. (e.g. My business is not running at all)

***Code BLUE***
Please plan out. (e.g. I can survive for now)

***Code GREEN***
Not urgent. (e.g. take your time)
-->



[comment]: # (Automation - Don't worry! Let's Cory takes over here.)
/label ~"Discussion" ~"Question"
