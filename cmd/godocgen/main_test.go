package main

import (
	"os"
	"testing"
)

func TestMain(t *testing.T) {
	os.Args = []string{"godocgen", "-h"}

	main()

	t.Logf("Ran main program with help option. It should not panic.")
}
