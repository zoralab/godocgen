module gitlab.com/zoralab/godocgen

go 1.13

replace gitlab.com/zoralab/godocgen => ./

require gitlab.com/zoralab/cerigo v0.0.1
