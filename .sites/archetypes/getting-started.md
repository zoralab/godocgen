{{- $titleWords := replace (replace .Name "-" " ") "_" " " -}}
{{- $keywords := split $titleWords " " -}}
{{- $keywords = $keywords | append "Godocgen" -}}
{{- $title := strings.Title $titleWords -}}

<!--
+++
date = "{{ .Date }}"
title = "{{- $title -}}"
description = """
{{ $title }} post description is here.
It will be shown in Google Search bar.
"""
keywords = {{ replace (printf "%+q" $keywords) " " ", " }}
authors = ["Godocgen Team"]
draft = true
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "A) Getting Started"
#name = "{{- $title -}}"
weight = 1
+++
-->
