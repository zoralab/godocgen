#!/bin/bash
##############################################################################
# GO AVAILABILITY CHECK
##############################################################################
if [ -z "$(type -p go)" ]; then
	echo "[ ERROR ] Go program is not detected. Is Go installed?"
	exit 1
fi

##############################################################################
# VARIABLES MANAGEMENTS
##############################################################################
export GO_PKG_PATH="$PWD"
export GO_LOG_PATH="${GO_LOG_PATH:-"${GO_PKG_PATH}/.log"}"
export GO_BENCH_TIMEOUT="${GO_BENCH_TIMEOUT:-"1s"}"
export GO_TEST_DEFAULT_ARGS="${GO_TEST_DEFAULT_ARGS:-"."}"
export GO_TEST_TIMEOUT="${GO_TEST_TIMEOUT:-"14400s"}" # 4 hours



goconfigRefresh() {
	case "$1" in
	-h|--help|help)
		echo "\
goconfigRefresh is a BASH function to refresh all the environment variables
after changing \$GO_LOG_PATH environment variable. To refresh, do:
  1. $ goconfigRefresh
"
		return 0
		;;
	esac

	mkdir -p "$GO_LOG_PATH"

	export GO_BENCH_BLOCK_OUTPUT="\
${GO_BENCH_BLOCK_OUTPUT:-"${GO_LOG_PATH}/gobenchmark_block.out"}"
	export GO_BENCH_BLOCK_SVG="\
${GO_BENCH_BLOCK_SVG:-"${GO_LOG_PATH}/gobenchmark_block.svg"}"
	export GO_BENCH_CPU_OUTPUT="\
${GO_BENCH_CPU_OUTPUT:-"${GO_LOG_PATH}/gobenchmark_cpu.out"}"
	export GO_BENCH_CPU_SVG="\
${GO_BENCH_CPU_SVG:-"${GO_LOG_PATH}/gobenchmark_cpu.svg"}"
	export GO_BENCH_LOG="\
${GO_BENCH_LOG:-"${GO_LOG_PATH}/gobenchmark.log"}"
	export GO_BENCH_LOG_DELTA="\
${GO_BENCH_LOG_DELTA:-"${GO_LOG_PATH}/gobenchmark_delta.log"}"
	export GO_BENCH_LOG_OLD="\
${GO_BENCH_LOG_OLD:-"${GO_LOG_PATH}/gobenchmark_old.log"}"
	export GO_BENCH_MEM_OUTPUT="\
${GO_BENCH_MEM_OUTPUT:-"${GO_LOG_PATH}/gobenchmark_mem.out"}"
	export GO_BENCH_MEM_SVG="\
${GO_BENCH_MEM_SVG:-"${GO_LOG_PATH}/gobenchmark_mem.svg"}"
	export GO_BENCH_MUTEX_OUTPUT="\
${GO_BENCH_MUTEX_OUTPUT:-"${GO_LOG_PATH}/gobenchmark_mutex.out"}"
	export GO_BENCH_MUTEX_SVG="\
${GO_BENCH_MUTEX_SVG:-"${GO_LOG_PATH}/gobenchmark_mutex.svg"}"
	export GO_CHECK_LOG="\
${GO_CHECK_LOG:-"${GO_LOG_PATH}/goCheck.log"}"
	export GO_DOCUMENT_LOG="\
${GO_DOCUMENT_LOG:-"${GO_LOG_PATH}/goDocument.log"}"
	export GO_TEST_HTML="\
${GO_TEST_HTML:-"${GO_LOG_PATH}/gotest.html"}"
	export GO_TEST_LOG="\
${GO_TEST_LOG:-"${GO_LOG_PATH}/gotest.log"}"
	export GO_TEST_OUTPUT="\
${GO_TEST_OUTPUT:-"${GO_LOG_PATH}/gotest.out"}"

	# seek linter
	export GO_LINTER=""
	if [ ! -z "$(type golangci-lint 2> /dev/null)" ]; then
		export GO_LINTER="golangci-lint"
	fi
}
export -f goconfigRefresh
goconfigRefresh



unsetAllAppVariables() {
	unset \
		GO_BENCH_BLOCK_OUTPUT \
		GO_BENCH_BLOCK_SVG \
		GO_BENCH_CPU_OUTPUT \
		GO_BENCH_CPU_SVG \
		GO_BENCH_LOG \
		GO_BENCH_LOG_DELTA \
		GO_BENCH_LOG_OLD \
		GO_BENCH_MEM_OUTPUT \
		GO_BENCH_MEM_SVG \
		GO_BENCH_MUTEX_OUTPUT \
		GO_BENCH_MUTEX_SVG \
		GO_BENCH_TIMEOUT \
		GO_CHECK_LOG \
		GO_LINTER \
		GO_LOG_PATH \
		GO_TEST_DEFAULT_ARGS \
		GO_TEST_HTML \
		GO_TEST_LOG \
		GO_TEST_OUTPUT \
		GO_TEST_TIMEOUT \
		ret
}

##############################################################################
# FUNCTIONS
##############################################################################
run_golangci_lint() {
	arg="${1:-.}"

	if [ -f "$arg" ]; then
		arg="${arg%/*}"
	fi

	if [ -d "$arg" ]; then
		arg="${arg}/..."
	fi


	oldIFS="$IFS" && IFS=$'\n'
	output=($(2>&1 golangci-lint run "$arg"))
	ret=$?
	IFS="$oldIFS" && unset oldIFS

	i=0
	for line in "${output[@]}"; do
		if [[ $line =~ .go:[[:digit:]]*: ]]; then
			if [ $i != 0 ]; then
				echo -e "\n\n" >> $GO_CHECK_LOG
			fi
		fi
		echo "$line" >> $GO_CHECK_LOG
		i=$((i + 1))
	done
	return $ret
}



goCheck() {
	open=1
	arg="${1:-.}"
	ret=0

	# check parameters
	case "$1" in
	-r)
		arg="${2:-.}"
		open=0
		;;
	-h|--help|help)
		echo "\
goCheck is a BASH function to execute the gofmt and golang-lint checking in Go.

By default, it needs a filepath. Supply the path directly after calling the
function. Example:
   1. $ goCheck source.go
   2. $ goCheck ./directory/source.go
   3. $ goCheck ./...
   4. $ goCheck ./directory/...

If you need to open the report automatically, supply -r flag as the first
argument. Example:
   1. $ goCheck -r source.go
   2. $ goCheck -r ./directory/source.go
   3. $ goCheck -r ./...
   4. $ goCheck -r ./directory/...
"
		return 0
		;;
	esac

	# gofmt source codes
	if [ -z "$(type gofmt 2> /dev/null)" ]; then
		1>&2 echo "[ ERROR ] missing gofmt. Is Go installed?"
		return 1
	fi

	if [ "${arg##*/}" == "..." ]; then
		arg="${arg%/*}"
	fi

	gofmt -w -s "$arg" &> "$GO_CHECK_LOG"

	ret=$?
	if [ $ret != 0 ]; then
		1>&2 echo "[ ERROR ] gofmt found errors. Check $GO_CHECK_LOG"
		unset arg open
		return $ret
	fi

	# add begin log
	echo "BEGIN CERIGO GOCHECK ANALYZER" >> "$GO_CHECK_LOG"
	printf '=%.0s' {1..79} >> "$GO_CHECK_LOG"
	echo "" >> "$GO_CHECK_LOG"

	# run static analysis
	case "$GO_LINTER" in
	golangci-lint)
		run_golangci_lint "$arg"
		ret=$?
		;;
	*)
		1>&2 echo "[ WARN  ] missing golangci-lint tool."
		echo "[ ERROR ] missing golangci-lint tool." >> "$GO_CHECK_LOG"
		;;
	esac

	# add end log
	printf '=%.0s' {1..79} >> "$GO_CHECK_LOG"
	echo "" >> "$GO_CHECK_LOG"
	echo "END CERIGO GOCHECK ANALYZER" >> "$GO_CHECK_LOG"

	# check open
	if [ $open -eq 0 ]; then
		xdg-open "$GO_CHECK_LOG" &> /dev/null
	fi
	unset arg open
	return $ret
}
export -f goCheck



goTest() {
	arg="${1:-"$GO_TEST_DEFAULT_ARGS"}"
	open=1
	ret=0

	# check parameters
	case "$1" in
	-r)
		arg="${2:-"$GO_TEST_DEFAULT_ARGS"}"
		open=0
		;;
	-h|--help|help)
		echo "\
goTest is a BASH function to execute the testing in Go.

By default, it refers to current directory. To customize the filepath, supply
the path directly after calling the function. Example:
   1. $ goTest ./...
   2. $ goTest ./path/package

If you need to open the report automatically, supply -r flag as the first
argument. Example:
   1. $ goTest -r ./...
   2. $ goTest -r ./path/package
"
		return 0
		;;
	esac

	# validate input
	if [ "${arg##*/}" != "..." ]; then
		ret=($(find $arg -maxdepth 1 -name "*.go"))
		if [ ${#ret[@]} -eq 0 ]; then
			echo "[ ERROR ] no go files in $arg directory"
			unset arg open ret
			return 1
		fi
	fi

	# run test
	go test -timeout "$GO_TEST_TIMEOUT" \
		-coverprofile "$GO_TEST_OUTPUT" \
		-race \
		-v "$arg" \
		| tee "$GO_TEST_LOG"
	ret=$?
	if [ $ret != 0 ]; then
		unset arg open
		return $ret
	fi

	# process test output
	if [ -f "$GO_TEST_OUTPUT" ]; then
		2>&1 go tool cover \
			-html="$GO_TEST_OUTPUT" \
			-o "$GO_TEST_HTML" \
			> /dev/null
	fi

	# process open
	if [ $open -eq 0 ]; then
		xdg-open "$GO_TEST_HTML" &> /dev/null
		xdg-open "$GO_TEST_LOG" &> /dev/null
	fi
	unset arg open
	return "$ret"
}
export -f goTest



goBenchmark() {
	arg="${1:-.}"
	open=1
	timeout="${2:-"1s"}"
	list="${3:-.}"
	ret=0

	# check parameters
	case "$1" in
	-r)
		open=0
		arg="${2:-.}"
		timeout="${3:-"$GO_BENCH_TIMEOUT"}"
		list="${4:-.}"
		;;
	-h|--help|help)
		echo "\
goBenchmark is a BASH function to execute the benchmarking in Go.

By default, it refers to current directory. To customize the filepath, supply
the path directly after calling the function. Example:
   1. $ goBenchmark
   2. $ goBenchmark .
   3. $ goBenchmark ./path/package

By default, goBenchmark uses 1s as the timeout value. To customize the timeout,
supply the timeout variable as the subsequent variables. You MUST supply
the filepath/directory path beforehand. Example:
   1. $ goBenchmark . 99999s
   2. $ goBenchmark ./path/package 99999s

By default, goBenchmark runs all benchmark functions. To run 1 specific
benchmark, supply the function name as the subsequent variables. You MUST
supply both filepath/directory path and timeout beforehand. Example:
   1. $ goBenchmark . 99999s FunctionName
   2. $ goBenchmark ./path/package 99999s FunctionName
   3. $ goBenchmark ./path/package \$GO_BENCH_TIMEOUT FunctionName

If you need to open the report using default application automatically, supply
-r flag as the first argument. Example:
   1. $ goBenchmark -r
   2. $ goBenchmark -r .
   3. $ goBenchmark -r . 99999s
   4. $ goBenchmark -r . 99999s FunctionName
   5. $ goBenchmark -r ./path/package
   6. $ goBenchmark -r ./path/package 99999s
   7. $ goBenchmark -r ./path/package 99999s FunctionName
"
		unset arg open timeout ret list
		return 0
		;;
	esac

	# validate input
	ret=($(find $arg -maxdepth 1 -name "*.go"))
	if [ ${#ret[@]} -eq 0 ]; then
		echo "[ ERROR ] no go files in $arg directory"
		unset arg open timeout ret list
		return 1
	fi

	# prepare benchmark environment
	if [ -f "$GO_BENCH_LOG" ]; then
		mv "$GO_BENCH_LOG" "$GO_BENCH_LOG_OLD"
	fi

	# run benchmark
	currentDirectory="${PWD}" && cd "$arg"
	go test -run=none \
		-bench="$list" \
		-benchmem \
		-benchtime "$timeout" \
		-blockprofile "$GO_BENCH_BLOCK_OUTPUT" \
		-cpuprofile "$GO_BENCH_CPU_OUTPUT" \
		-memprofile "$GO_BENCH_MEM_OUTPUT" \
		-mutexprofile "$GO_BENCH_MUTEX_OUTPUT" \
		| tee "$GO_BENCH_LOG"
	ret=$?
	cd "$currentDirectory" && unset currentDirectory
	if [ $ret != 0 ]; then
		unset arg open timeout list
		return $ret
	fi

	# processing output
	if [ -f "$GO_BENCH_BLOCK_OUTPUT" ]; then
		2>&1 go tool pprof \
			-output "$GO_BENCH_BLOCK_SVG" \
			-svg "$GO_BENCH_BLOCK_OUTPUT" \
			> /dev/null
	fi
	if [ -f "$GO_BENCH_CPU_OUTPUT" ]; then
		2>&1 go tool pprof \
			-output "$GO_BENCH_CPU_SVG" \
			-svg "$GO_BENCH_CPU_OUTPUT" \
			> /dev/null
	fi
	if [ -f "$GO_BENCH_MEM_OUTPUT" ]; then
		2>&1 go tool pprof \
			-output "$GO_BENCH_MEM_SVG" \
			-svg "$GO_BENCH_MEM_OUTPUT" \
			> /dev/null
	fi
	if [ -f "$GO_BENCH_MUTEX_OUTPUT" ]; then
		2>&1 go tool pprof \
			-output "$GO_BENCH_MUTEX_SVG" \
			-svg "$GO_BENCH_MUTEX_OUTPUT" \
			> /dev/null
	fi
	arg="$(type benchcmp 2> /dev/null)"
	if [ "$arg" == "" ] && \
		[ -f "$GO_BENCH_LOG_OLD"] && \
		[ -f "$GO_BENCH_LOG" ]; then
		2>&1 benchcmp "$GO_BENCH_LOG_OLD" "$GO_BENCH_LOG" \
			> "$GO_BENCH_LOG_DELTA"
		ret=$?
		if [ $open -eq 0 ]; then
			xdg-open "$GO_BENCH_LOG_DELTA" &> /dev/null
		fi
	fi

	# check open insturction
	if [ $open -eq 0 ]; then
		xdg-open "$GO_BENCH_LOG" &> /dev/null
		xdg-open "$GO_BENCH_BLOCK_SVG" &> /dev/null
		xdg-open "$GO_BENCH_CPU_SVG" &> /dev/null
		xdg-open "$GO_BENCH_MEM_SVG" &> /dev/null
		xdg-open "$GO_BENCH_MUTEX_SVG" &> /dev/null
	fi
	unset arg open timeout ret list
	return $ret
}
export -f goBenchmark



goDocument() {
	arg="${1:-.}"
	open=1

	# check parameters
	case "$1" in
	-h|--help|help)
		echo "\
goDocument is to list out a package's documentation.
Example:
   1. $ goDocument
   1. $ goDocument .
   2. $ goDocument ./path/to/package

If you need to open the report using default application automatically, supply
-r flag as the first argument. Example:
   1. $ goDocument -r
   2. $ goDocument -r .
   3. $ goDocument -r ./path/to/package
"
		unset arg
		return 0
		;;
	-r)
		open=0
		arg="${2:-.}"
		;;
	esac

	go doc --all "$arg" >> "$GO_DOCUMENT_LOG"

	# check open insturction
	if [ $open -eq 0 ]; then
		xdg-open "$GO_DOCUMENT_LOG" &> /dev/null
	fi

	unset arg open
	return 0
}
export -f goDocument



goClean() {
	arg="${1:-.}"

	# check parameters
	case "$1" in
	-h|--help|help)
		echo "\
goClean is a BASH function to clean up the repository from compiled objects.

Currently, goClean removes the following files:
  1. *.test
  2. *.out
  3. *.d
  4. *.o
  5. *.elf
  6. *.hex
  7. *.pyc
  8. *.bin
  9. \$GO_LOG_PATH/*

By default, it refers to current directory. To customize the filepath, supply
the path directly after calling the function. Example:
  1. $ goClean
  2. $ goClean .
  3. $ goClean ./path/package
"
		unset arg
		return 0
		;;
	esac

	# clean
	find "$arg" \( \
		-name '*.test' -o \
		-name '*.out' -o \
		-name '*.d' -o \
		-name '*.o' \
		-name '*.elf' -o \
		-name '*.hex' -o \
		-name '*.pyc' -o \
		-name '*.bin' \
		\) \
		-type f \
		-delete
	rm -rf "${GO_LOG_PATH}/*"
	unset arg
	return 0
}
export -f goClean



goUnset() {
	# check parameters
	case "$1" in
	-h|--help|help)
		echo "\
goUnset is a BASH function to remove the goconfig.* traces, restoring the
terminal back into its origial conditions.
Example:
   1. $ goUnset
"
		unset arg
		return 0
		;;
	esac

	rm -rf "$GO_LOG_PATH"
	unsetAllAppVariables
	unset \
		goCheck \
		goTest \
		goBenchmark \
		goClean \
		goDocument \
		goUnset
}
export -f goUnset
