#!/bin/bash
if [ ! -f "./go.mod" ]; then
	1>&2 echo "[ ERROR ] not in root directory."
	exit 1
fi

go run ./cmd/godocgen/main.go \
	--output ./docs/en-us/go-doc \
	--input ./... \
	--filename="_index.md" \
	--template ./.godocgen/templates/md
