package godocgen

// App constants are meant for configuring godocgen to work in a specific
// manner
const (
	// VERSION is the version number godocgen is current are.
	VERSION = "0.0.2"

	// DefaultFilename is the default filename in the event where filename
	// is left out when it is needed.
	DefaultFilename = "index.txt"
)

// Supported file extension formats
const (
	// Markdown file extension
	Markdown = ".md"

	// Terminal
	Terminal = "terminal"

	// Text file extension
	Text = ".txt"
)

// Working modes
const (
	// AppMode is to set the app to operate as normal app mode.
	// It allows one not only to generate the package documentation
	// data but also rendering the output.
	AppMode = uint(0)

	// MockMode is the set the app to operate as a mocking medium.
	// This is useful for developers to unit-test his/her own packages
	// without needing to write too many mock codes when using godocgen
	// as `LibraryMode`. MockMode will make `App` exits immediately after
	// initialized, allowing developer to mock the elements inside it.
	MockMode = uint(1)

	// LibraryMode is the set the app to operate as a Go package library.
	// This allows developer to import godocgen to generate the packages'
	// documentations data without needing to render them out. That being
	// said, LibraryMode will not run rendering at all.
	LibraryMode = uint(2)
)
