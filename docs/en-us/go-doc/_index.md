<!--
+++
# WARNING: This markdown is autogenerated by bot. Do not edit it manually!
date = "2020-04-17T01:55:16+08:00"
title = "godocgen"
description = """
Package godocgen is to generate Go module package's documentations.

Its primary intension was to restore the ability to host the documents via
a web server for Go Modules packages using other third-party hosting
solutions. With Godocgen, one can generate the web materials (e.g. Markdown
or HTML) and freely use his/her own hosting solution to publish the module's
documentations. Long story short, Godocgen facilitates freedom to developer
while not compromising one of Go's beauty of auto-documentation feature.

**MINIMUM VERSIONS**

Godocgen should be used with `Go 1.14.1` and above and it's not backwards
compatible (Reason: Example rendering API only available starting from
`Go 1.14.1`).

"""
keywords = [ "godocgen", "go" ]
authors = [ "Godocgen Team" ]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "X) Go Doc"
name = "godocgen"
weight = 1
+++
-->
# Package godocgen
```go
import "gitlab.com/zoralab/godocgen"
```
Package godocgen is to generate Go module package's documentations.

Its primary intension was to restore the ability to host the documents via
a web server for Go Modules packages using other third-party hosting
solutions. With Godocgen, one can generate the web materials (e.g. Markdown
or HTML) and freely use his/her own hosting solution to publish the module's
documentations. Long story short, Godocgen facilitates freedom to developer
while not compromising one of Go's beauty of auto-documentation feature.

**MINIMUM VERSIONS**

Godocgen should be used with `Go 1.14.1` and above and it's not backwards
compatible (Reason: Example rendering API only available starting from
`Go 1.14.1`).





## Constants
App constants are meant for configuring godocgen to work in a specific
manner
```go
const (
	// VERSION is the version number godocgen is current are.
	VERSION = "0.0.1"


	// DefaultFilename is the default filename in the event
	// where filename is left out when it is needed.
	DefaultFilename = "index.txt"
)
```

Supported file extension formats
```go
const (
	// Markdown file extension
	Markdown = ".md"


	// Terminal
	Terminal = "terminal"


	// Text file extension
	Text = ".txt"
)
```

Working modes
```go
const (
	// AppMode is to set the app to operate as normal app mode.
	// It allows one not only to generate the package
	// documentation data but also rendering the output.
	AppMode = uint(0)


	// MockMode is the set the app to operate as a mocking
	// medium. This is useful for developers to unit-test his/her
	// own packages without needing to write too many mock codes
	// when using godocgen as `LibraryMode`. MockMode will make
	// `App` exits immediately after initialized, allowing
	// developer to mock the elements inside it.
	MockMode = uint(1)


	// LibraryMode is the set the app to operate as a Go package
	// library. This allows developer to import godocgen to
	// generate the packages' documentations data without needing
	// to render them out. That being said, LibraryMode will not
	// run rendering at all.
	LibraryMode = uint(2)
)
```

Error constants are the values used for error identifications via
application programming.
```go
const (
	// ErrorTag is the error message tag for printing out error
	// messages.
	ErrorTag = "ERROR"


	// WarningTag is the warning message tag for printing out
	// less critical error messages.
	WarningTag = "WARNING"


	// ErrorCodeNone is the error code that has no errors.
	ErrorCodeNone = 0


	// ErrorCodeCommon is the error code for common errors.
	ErrorCodeCommon = 1
)
```

Error messages are the string values used for error object text output.
```go
const (
	// ErrorBadInputPath is the error message for a completely
	// bad and unusable input path.
	ErrorBadInputPath = "bad input path"


	// ErrorBadOutputPath is the error message for a completely
	// bad and unusable output path.
	ErrorBadOutputPath = "bad output path"


	// ErrorBadTemplatePath is the error message for a
	// completely bad and unusable template path.
	ErrorBadTemplatePath = "bad template path"


	// ErrorConfigureDocData is the error message for unable to
	// configure package's raw DocData
	ErrorConfigureDocData = "unable to configure raw DocData"


	// ErrorMissingInputPath is the error message for completely
	// missing input path.
	ErrorMissingInputPath = "missing input path"


	// ErrorMissingOutputPath is the error message for
	// completely missing output path.
	ErrorMissingOutputPath = "missing output path"


	// ErrorMissingTemplatePath is the error message for
	// completely missing template path.
	ErrorMissingTemplatePath = "missing template path"


	// ErrorInputPathNotDirectory is the error message for the
	// given input path is not a directory.
	ErrorInputPathNotDirectory = "given input path is not a directory"


	// ErrorInputPathWithoutGo is the error message for the
	// given input path does not contain any directory.
	ErrorInputPathWithoutGo = "given input path has no go packages"


	// ErrorOutputPathIsAFile is the error message for the given
	// output path is a file and is not ready for output usage.
	ErrorOutputPathIsAFile = "output path is a file"


	// ErrorOutputPermission is the error message for unable to
	// read/write into output.
	ErrorOutputPermission = "unable to read/write to output"


	// ErrorTemplatePathIsADir is the error message for the
	// given output path is a directory and is not a file for
	// template rendering.
	ErrorTemplatePathIsADir = "template path is a directory"


	// ErrorInTemplate is the error message for a given template
	// having its internal errors.
	ErrorInTemplate = "template is having error"


	// ErrorUnknownOutputFormat is the error message for
	// unidentifiable filetype based on the given filename.
	ErrorUnknownOutputFormat = "unknown filetype from the given filename"


	// ErrorUnknownWorkMode is the error message for
	// unidentifiable work mode.
	ErrorUnknownWorkMode = "unknown work mode"
)
```





## App
```go
type App struct {
	// user input
	InputPath    string
	OutputPath   string
	Filename     string
	TemplatePath string
	WorkMode     uint
	Width        uint

	// generated output
	Targets  []*Data
	Log      []string
	ExitCode int
	// contains filtered or unexported fields
}
```
App is the master control for godocgen go package.

It has private variables that require initialization. Therefore, to create
one safely, please use `NewApp()` function.

Before calling its `App.Run()` function, you need to ensure the following
elements are fulfilled.

COMPULSORY elements:
  1. `OutputPath`   - location for storing the generated output (directory).
  2. `InputPath`    - location for reading the go packages (directory).
                      Append `...` in the end for recursive generations.
  3. `WorkMode`     - how App should works such as: as a test mocking unit,
                      package library, or as app mode.

OPTIONAL elements:
  1. `TemplatePath` - location to the template file for output rendering.
  2. `Filename`     - output filename with extension. If this is set,
                      `TemplatePath` must be specified.
  3. `Width`        - to specify the maximum width of the document. Default
                      is `80` for terminal and `70` for files.

For OUTPUT, depending on `App.WorkMode`, they are different. For example,
  1. in `godocgen.MockMode`, nothing is executed. `App` exited immediately.
  2. in `godocgen.LibraryMode`, the output, system log, and exit code are
     stored inside `App.Targets`, `App.Log`, and `App.Exit` respectively.
     Rendering process is not executed in this mode, allowing one to use
     `App.Targets` `Data` list for his/her own Go rendering.
  3. in `godocgen.AppMode`, it will run the full-fletch executions including
     rendering.



### `Func NewApp() *App`
NewApp is the function for creating the godocgen App structure object.

It returns the memory pointer of the created App object. Since App structure
contains uninitialized private elements, it is best to use this function to
create App object.



### `Func (c *App) Run()`
Run is the function for starting the godocgen process.

It executes the processing accordingly based on its WorkingMode. For AppMode,
the rendering is inclusive and will be rendered automatically.





## Data
```go
type Data struct {
	Header       string
	ImportPath   string
	Synopsis     string
	RelativePath string
	Filepath     string
	Codes        []string
	Examples     *DataElement
	Constants    *DataElement
	Variables    *DataElement
	Functions    *DataElement
	Methods      *DataElement
	Types        []*Data
}
```
Data is the data structure holding `Package` or `Type` level.

This structure contains elements that needs initialization. Hence, to be
on the safe side, you should use `NewData()` function if you ever need to
create one.

For `Package` subject, the elements' representations are as follow:
1. `Header`       - name of the package
2. `ImportPath`   - the import path statement
3. `Synopsis`     - the descriptions of the package
4. `RelativePath` - the relative path of the package to its root directory
5. `Filepath`     - the output filepath (for file rendering)
6. `Codes`        - not applicable: either `nil` or empty
7. `Examples`     - the element holding the `List` of examples
8. `Constants`    - the element holding the `List` of constants
9. `Variables`    - the element holding the `List` of variables
10. `Functions`   - the element holding the `List` of functions
11. `Methods`     - not applicable: either `nil` or empty
12. `Types`       - array of `Data` holding `Type` details

For `Type` subject, the elements' representations are as follow:
1. `Header`       - name of the `Type`
2. `ImportPath`   - not applicable: it is always empty
3. `Synopsis`     - the descriptions of the `Type`
4. `RelativePath` - not applicable: it is always empty
5. `Filepath`     - not applicable: it is always empty
6. `Codes`        - Codes in multiple line
7. `Examples`     - the element holding the `List` of examples
8. `Constants`    - the element holding the `List` of constants
9. `Variables`    - the element holding the `List` of variables
10. `Functions`   - the element holding the `List` of functions
11. `Methods`     - the element holding the `List` of functions with receiver
12. `Types`       - not applicable: either `nil` or empty



### `Func NewData() *Data`
NewData creates a new Data object and return its pointer as output.

This is the recommended function to create new *Data structure type over the
conventional `&Data{...}` method.

Unless you're using App as LibraryMode for development or testing, you do
not need to use this function since `App` will generate the `Data` objects
automatically. This function was meant to facilitate Developers to create
mocking data for unit testing.





## DataElement
```go
type DataElement struct {
	Header   string
	Synopsis string
	Receiver string
	Value    string
	Codes    []string
	Examples []*DataElement
	List     []*DataElement
}
```
DataElement is the data structure holding specific data values for `Data`.

The structure contains elements that require initialization. Hence, it is
not safe to create using `struct{}` method.

Depending on its associated relationship with its `Data` structure,
DataElement can be represented in many ways, including towards itself.

----

For `Examples` association from `Data`:
  1. `DataElement`'s `Examples` holds all the `Data`'s examples in its
     `List`, which is called **Examples Group**.
  2. For each elements in **Examples Group**'s `List`, it is called
     **Example Elements**.

  EXAMPLES GROUP
  * `Header`   - not applicable: it is empty
  * `Receiver` - not applicable: it is empty
  * `Synopsis` - the synopsis of the examples
  * `Value`    - not applicable: it is empty
  * `Codes`    - not applicable: either `nil` or empty
  * `Examples` - list of **Example Elements**.
  * `List`     - not applicable: either `nil` or empty

  EXAMPLES ELEMENTS
  * `Header`   - suffix of the example (`_second`, `_third`, etc.)
  * `Receiver` - name of the example with trimmed suffix
  * `Synopsis` - the descriptions of the example
  * `Value`    - output value of the example
  * `Codes`    - the lines of codes describing the example (trimmed newline
                 and tab characters)
  * `Examples` - not applicable: either `nil` or empty
  * `List`     - not applicable: either `nil` or empty

----

For `Constants` and `Variables` association from `Data`:
  1. `DataElement`'s `List` holds all the `Data`'s constants/variables group
     in a list of `DataElements` called **Constants/Variables Group**.
  2. In each "constant/variable group", its `List` has all the
     constants/variables called **Constant/Variable Elements**.

  CONSTANT/VARIABLES GROUP
  * `Header`   - group type in code form. Either `const` or `var`
  * `Receiver` - not applicable: it is empty
  * `Synopsis` - the description of the example group
  * `Value`    - not applicable: it is empty
  * `Codes`    - not applicable: either `nil` or empty
  * `Examples` - not applicable: either `nil` or empty
  * `List`     - list of **Constant/variable elements**

  CONSTANT/VARIABLE ELEMENTS
  * `Header`   - suffix of the example (`_second`, `_third`, etc.)
  * `Receiver` - name of the example with trimmed suffix
  * `Synopsis` - the descriptions of the example
  * `Value`    - output value of the example
  * `Codes`    - the lines of codes describing the example (trimmed newline
                 and tab characters)
  * `Examples` - not applicable: either `nil` or empty
  * `List`     - not applicable: either `nil` or empty

----

For `Functions` and `Methods` association from `Data`:
  1. The `Functions`/`Methods` holds all its functions/methods in its `List`
     called **Functions/Methods Group**.
  2. For each elements in **Functions/Methods Group**, it holds the data for
     the function/method called **Function/Method Elements**.

  FUNCTIONS/METHODS GROUP
  * `Header`   - not applicable: it is empty
  * `Receiver` - not applicable: it is empty
  * `Synopsis` - the description of the functions/methods group
  * `Value`    - not applicable: it is empty
  * `Codes`    - not applicable: either `nil` or empty
  * `Examples` - not applicable: either `nil` or empty
  * `List`     - list of **Function Elements**

  FUNCTION/METHOD ELEMENTS
  * `Header`   - synopsis of the functions
  * `Receiver` - Only applicable for `method` which is function receiver type
  * `Synopsis` - not applicable: it is empty
  * `Value`    - function name with `Receiver` if exists, without `func`
                 keyword)
  * `Codes`    - not applicable: either `nil` or empty
  * `Examples` - list of associated examples
  * `List`     - not applicable: either `nil` or empty
