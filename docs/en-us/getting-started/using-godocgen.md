<!--
+++
date = "2020-04-01T11:48:59+08:00"
title = "Using Godocgen"
description = """
Stuck with Godocgen? This page will show you how to use it easily for generating
Go documentations especially with module packages.
"""
keywords = ["using", "Godocgen"]
authors = ["Godocgen Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "A) Getting Started"
#name = "Using Godocgen"
weight = 1
+++
-->
# Using Godocgen
Now that you have Godocgen installed in your system, you can now proceed to
use Godocgen.

Godocgen strives to use **parameters-only** configurations to simplify its
user interfaces, making the communications and supports easier to use.




## Basic
To simply output a Go package, you can just provide `--output` and `--input`
arguments.


### To Terminal Output
This will generate the documentation from the current directory (`.`) and dump
the output to the terminal.

You only need to supply `terminal` as output and use `--input` to point towards
your targeted package. Here is an example:

```bash
$ godocgen "--output terminal" --input "."
```


### To File Output
This will generate the documentation from the current directory (`.`) and create
the output to `/path/to/output/dir/index.txt`.

You only need to supply the directory path for the output file. Here is an
example:

```bash
$ godocgen --output "/path/to/output/dir" --input "."
```

To change the generated filename and filetype, see
[Using Template]({{< absLangLink "templates/general" >}}) section.




## Advanced Usage
Now that the basic are cleared, it's time to opt for Godocgen advanced usage.


### Entire Go Packages (including Sub-Directories)
This will generate the documentation recursively starting from the current
directory (`.`). The output is structured based on the sub-directory pathing
and save them into `/path/to/output/dir`. For example,

1. If a Go package is located at `/path/to/output/dir`, then its output is
   saved into `/path/to/output/dir/index.txt`.
2. If a Go package is located at `/path/to/output/dir/internal/pack`, then its
   output is saved into `/path/to/output/dir/internal/pack/index.txt`.

You only need to append the Go recursive indicator, the triple dots (`...`) at
the end of the input. Here is the example command:

```bash
$ godocgen --output "/path/to/output/dir" --input "./..."
```

> **NOTE**: you can output it to `terminal` but that will append everything into
> a single output. That being said, it defeats the Godocgen main purpose anyway.

To change the generated filename and filetype, see
[Using Template]({{< absLangLink "templates/general" >}}) section.


### Using Templates
To use template, visit the dedicated section:
> [Template - in General]({{< absLangLink "templates/general" >}})
