<!--
+++
date = "2020-03-16T18:40:12+08:00"
title = "Contributors"
description = """
This section covers all the Bissetii's contributors ranging from codes
submission to financial support.
"""
keywords = ["godocgen", "contributors", "list"]
authors = [ "Godocgen" ]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "Y) Projects"
weight = 1
+++
-->
# Contributors
Here are the list of project contributors and sponsors to make Godocgen Project
available as open-source tool.

## Sponsors
[![ZORALab]({{< absLink "img/sponsors/zoralab.png" >}})](https://www.zoralab.com)

> Want to be here? See [Sponsoring]({{< absLangLink "/projects/sponsoring/" >}}).


## Contributors
Here are the list of contributors base on their roles and contributions. The
maintainers are the one that maintains and manage the package releases.

### Maintainers
1. (Holloway) Chew, Kean Ho (`kean.ho.chew@zoralab.com`) - 2020 to present

### Developers
1. (Holloway) Chew, Kean Ho (`kean.ho.chew@zoralab.com`) - 2020 to present
