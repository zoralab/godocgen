<!--
+++
date = "2020-03-16T15:27:20+08:00"
title = "Godocgen Project"
description = """
Godocgen Project is the documentation generator for Go module packages. This
facilitates documentations using custom renderer.
"""
keywords = ["godocgen", "go", "documentation"]
authors = [ "ZORALab", "Holloway Chew Kean Ho" ]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
name = "Home"
parent = ""
weight = 1
+++
-->

# Godocgen

![Godocgen]({{< absLink "img/logo/banner.svg" >}})

Godocgen is an app built using Go programming language to generate Go module
package's documentations. It parses the packages documentation data and
facilitates custom rendering, enabling Gopher to use other hosting solution
like [Hugo](https://gohugo.io/) to host the documents.

Feel free to explore the documentation site here! In case you need a direction,
please start here:

> **[Getting Started Step 1]({{< absLangLink "getting-started/install/" >}})**

If you're looking for source code, please do check it out at:

> **[https://gitlab.com/ZORALab/godocgen](https://gitlab.com/ZORALab/godocgen)**


## Features
Godogen has the following features:

1. ✅Support terminal printout. (`v0.0.1`)
2. ✅Support `text/template` documentations rendering. (`v0.0.1`)
3. ✅Support Hugo markdown generation. (`v0.0.1`)
4. ✅Support directory index structure. (`v0.0.1`)
5. 📅Support `html/template` documentations rendering.
6. ✅Support `Go Get` installation. (`v0.0.1`)
7. 📅Support DEB Server for Debian based Linux rolling release.
8. 📅Support RPM Server for RPM based Linux rolling release.
9. ✅Unit-tested. (`v0.0.2`)
10. ✅Support Examples rendering. (`v0.0.2`)

| Legends | Descriptions                                |
|:--------|:--------------------------------------------|
| ✅      | Completed.                                  |
| 📅      | Confirmed Proposal. Currently Developing... |
| ❌      | Rejected Idea.                              |
| 💡      | Considered Ideas. Still Discussing...       |

For more information, please check out the Issues board at:

> [https://gitlab.com/ZORALab/godocgen/issues](https://gitlab.com/ZORALab/godocgen/issues)


## Sponsorship
This project is sponsored by:

[![ZORALab]({{< absLink "img/sponsors/zoralab.png" >}})](https://www.zoralab.com)


## Development Health
This section covers the current status of the Git development repository.

| Branch   | Test Status | Test Coverage |
|:---------|:------------|:--------------|
| `master` | ![pipeline status](https://gitlab.com/ZORALab/godocgen/badges/master/pipeline.svg) | ![coverage report](https://gitlab.com/ZORALab/godocgen/badges/master/coverage.svg) |
| `staging`   | ![pipeline status](https://gitlab.com/ZORALab/godocgen/badges/staging/pipeline.svg) | ![coverage report](https://gitlab.com/ZORALab/godocgen/badges/staging/coverage.svg) |
| `next`   | ![pipeline status](https://gitlab.com/ZORALab/godocgen/badges/next/pipeline.svg) | ![coverage report](https://gitlab.com/ZORALab/godocgen/badges/next/coverage.svg) |


## Primary Tools
{{< svg/shieldTag "Core Tools" "Hugo" "#1313c3" "#ffffff" >}}
{{< svg/shieldTag "Core Tools" "Go" "#1313c3" "#ffffff" >}}
{{< svg/shieldTag "License" "Apache 2.0" "#ff4500" "#ffffff" >}}
