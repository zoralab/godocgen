<!--
+++
date = "2020-03-16T15:27:20+08:00"
title = "Godocgen 工程"
description = """
Godocgen 是个自动写出Go Module去包文件的Go APP。它可以把Go Module去包文件自动写
成第三方的文件模式（比如Markdown），好让去包主人运用其他网站科技来主持文件网站。
"""
keywords = ["godocgen", "go", "文件"]
authors = [ "ZORALab", "Holloway Chew Kean Ho" ]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
name = "主页"
parent = ""
weight = 1
+++
-->

# ![Godocgen]({{< absLink "img/logo/banner.svg" >}})
Godocgen 是个自动写出Go Module去包文件的Go APP。它可以把Go Module去包文件自动写
成第三方的文件模式（比如Markdown），好让去包主人运用其他网站科技来主持文件网站。

这是Godocgen的Go去包文件网站。目前Godocgen暂时运用英语来表达它现任的科技模式。
请多多包涵。英语网站地址在这儿：

> [{{< absLink "/en-us" >}}]({{< absLink "/en-us" >}})


## 资金赞助
Godogen是由以下的公司赞助：

[![ZORALab]({{< absLink "img/sponsors/zoralab.png" >}})](https://www.zoralab.com)


## 建设状况
这是目前Godocgen的建设状况：

| Branch   | Test Status | Test Coverage |
|:---------|:------------|:--------------|
| `master` | ![pipeline status](https://gitlab.com/ZORALab/godocgen/badges/master/pipeline.svg) | ![coverage report](https://gitlab.com/ZORALab/godocgen/badges/master/coverage.svg) |
| `staging`   | ![pipeline status](https://gitlab.com/ZORALab/godocgen/badges/staging/pipeline.svg) | ![coverage report](https://gitlab.com/ZORALab/godocgen/badges/staging/coverage.svg) |
| `next`   | ![pipeline status](https://gitlab.com/ZORALab/godocgen/badges/next/pipeline.svg) | ![coverage report](https://gitlab.com/ZORALab/godocgen/badges/next/coverage.svg) |


## 首要科技
{{< svg/shieldTag "主要科技" "Hugo" "#1313c3" "#ffffff" >}}
{{< svg/shieldTag "主要科技" "Go" "#1313c3" "#ffffff" >}}
{{< svg/shieldTag "运用执照" "Apache 2.0" "#ff4500" "#ffffff" >}}
