package cmdline

func getTestScenarios() []testScenario {
	return []testScenario{
		{
			UID:      1,
			TestType: testParse,
			Description: `
Parse is able to parse help argument properly. If there is no panic-like error
occurs, it means the parsing works successfully.
`,
			Switches: map[string]bool{
				simulateShortHelpArgument: true,
			},
		}, {
			UID:      2,
			TestType: testParse,
			Description: `
Parse is able to parse help argument properly. If there is no panic-like error
occurs, it means the parsing works successfully.
`,
			Switches: map[string]bool{
				simulateLongHelpArgument: true,
			},
		}, {
			UID:      3,
			TestType: testParse,
			Description: `
Parse is able to parse version argument properly. If there is no panic-like
error occurs, it means the parsing works successfully.
`,
			Switches: map[string]bool{
				simulateShortVersionArgument: true,
			},
		}, {
			UID:      4,
			TestType: testParse,
			Description: `
Parse is able to parse version argument properly. If there is no panic-like
error occurs, it means the parsing works successfully.
`,
			Switches: map[string]bool{
				simulateLongVersionArgument: true,
			},
		}, {
			UID:      5,
			TestType: testParse,
			Description: `
Parse is able to parse normal argument properly. If there is no panic-like
error occurs, it means the parsing works successfully.
`,
			Switches: map[string]bool{},
		},
	}
}
