package cmdline

import (
	"os"
	"testing"

	"gitlab.com/zoralab/cerigo/testing/thelper"
)

const (
	testParse = "testParse"
)

const (
	simulateLongHelpArgument     = "simulateLongHelpArgument"
	simulateLongVersionArgument  = "simulateLongVersionArgument"
	simulateShortHelpArgument    = "simulateShortHelpArgument"
	simulateShortVersionArgument = "simulateLongVersionArgument"
)

type testScenario thelper.Scenario

func (s *testScenario) prepareTHelper(t *testing.T) *thelper.THelper {
	return thelper.NewTHelper(t)
}

func (s *testScenario) log(th *thelper.THelper, data map[string]interface{}) {
	th.LogScenario(thelper.Scenario(*s), data)
}

func (s *testScenario) simulateInput() {
	switch {
	case s.Switches[simulateShortHelpArgument]:
		os.Args = []string{"godocgen", "-h"}
	case s.Switches[simulateLongHelpArgument]:
		os.Args = []string{"godocgen", "--help"}
	case s.Switches[simulateShortVersionArgument]:
		os.Args = []string{"godocgen", "-v"}
	case s.Switches[simulateLongVersionArgument]:
		os.Args = []string{"godocgen", "--version"}
	default:
		os.Args = []string{"godocgen",
			"--output",
			"/tmp/test.txt",
			"--template",
			"../../.godocgen/templates/md",
			"--input",
			"."}
	}
}
