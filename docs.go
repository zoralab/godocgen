// Package godocgen is to generate Go module package's documentations.
//
// Its primary intension was to restore the ability to host the documents via
// a web server for Go Modules packages using other third-party hosting
// solutions. With Godocgen, one can generate the web materials (e.g. Markdown
// or HTML) and freely use his/her own hosting solution to publish the module's
// documentations. Long story short, Godocgen facilitates freedom to developer
// while not compromising one of Go's beauty of auto-documentation feature.
//
// **MINIMUM VERSIONS**
//
// Godocgen should be used with `Go 1.14.1` and above and it's not backwards
// compatible (Reason: Example rendering API only available starting from
// `Go 1.14.1`).
package godocgen
